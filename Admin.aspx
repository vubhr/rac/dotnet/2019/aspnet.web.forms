﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    EnableEventValidation="false" CodeBehind="Admin.aspx.cs" Inherits="demo.app._2.csharp.Admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        <asp:Label ID="lblFormTitle" runat="server" Text="Kreiraj novog korisnika"></asp:Label></h1>
    
    <div class="form-group">
        <asp:HiddenField ID="hdnUserId" runat="server" />
        <label for="exampleInputEmail1">Username</label>
        <asp:TextBox ID="txtUsername" runat="server" CssClass="form-control"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvUsernameValidator" runat="server" ControlToValidate="txtUsername"
             ErrorMessage="Morate unesti korisničko ime"></asp:RequiredFieldValidator>
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">First name</label>
        <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control"></asp:TextBox>
    </div>
    <div class="form-group form-check">
         <label for="exampleInputEmail1">Last name</label>
        <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control"></asp:TextBox>
    </div>
    <div class="form-group">
         <label for="exampleInputEmail1">Role</label>
        <asp:DropDownList ID="ddlRole" runat="server">
        </asp:DropDownList>
    </div>
    <asp:Button ID="btnCreateUpdateUser" runat="server" Text="Create" OnClick="btnCreateUpdateUser_Click" />
    

    <asp:GridView ID="gvUsers" CssClass="table" runat="server" AutoGenerateColumns="false">
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="Id korisnika" />
            <asp:BoundField DataField="Username" HeaderText="Korisničko ime" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button ID="btnEditUser" runat="server" Text="Edit" CausesValidation="false"
                        OnClick="btnEditUser_Click" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id") %>' />
                    <asp:Button ID="btnDeleteUser" 
                            OnClientClick="javascript:return confirm('Jeste li sigurni da želite obrisati ovog korisnika?')" 
                            runat="server" Text="Delete" CausesValidation="false"
                            OnClick="btnDeleteUser_Click"
                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id") %>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
