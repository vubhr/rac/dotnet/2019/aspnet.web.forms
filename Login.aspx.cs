﻿using demo.app._2.csharp.Models;
using demo.app._2.csharp.Persistence;
using demo.app._2.csharp.Persistence.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace demo.app._2.csharp
{
    public partial class Login : System.Web.UI.Page
    {
        private IUserRepository _usersRepository;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            _usersRepository = new SqlLiteUserDapperRepository();
      
            pnlPoruke.Visible = false;
            lblPoruka.Text = string.Empty;
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            var username = txtUsername.Text;
            var password = txtPassword.Text;

            var foundUser = _usersRepository.Login(username, password);

            if (foundUser != null)
            {
                Session["user"] = foundUser;
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                pnlPoruke.Visible = true;
                lblPoruka.Text = "Neispravno korisničko ime ili lozinka";
            }

        }
    }
}