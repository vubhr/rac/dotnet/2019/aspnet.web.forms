﻿using demo.app._2.csharp.Models;
using demo.app._2.csharp.Persistence;
using demo.app._2.csharp.Persistence.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace demo.app._2.csharp
{
    public partial class SiteMaster : MasterPage
    {
        private IUserRepository _userRepository;
        protected void Page_Load(object sender, EventArgs e)
        {
            _userRepository = new SqlLiteUserDapperRepository();
            var user = Session["user"] as User;


            if(user != null) 
            {
                // Admin
                //if (user.RoleId == 2)
                if (user.Role.Id == _userRepository.GetRoles().Where(role => role.Name == "Admin").FirstOrDefault().Id)
                {
                    lnkAdminPage.Visible = true;
                }
            }
  
        }
    }
}