﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="Cart.aspx.cs" Inherits="demo.app._2.csharp.Cart" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h1>Proizvodi</h1>
    <asp:GridView ID="gvCart" runat="server" AutoGenerateColumns="false">
        <Columns>
            <asp:BoundField DataField="Title" HeaderText="Naziv proizvoda" />
            <asp:BoundField DataField="Quantity" HeaderText="Količina" />
            <asp:BoundField DataField="Price" HeaderText="Ukupna cijena" />     
          <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button ID="btnRemoveFromShoppingCart" runat="server" CausesValidation="false" Text="Ukloni" 
                        OnClick="btnRemoveFromShoppingCart_Click" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id") %>' />

                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
