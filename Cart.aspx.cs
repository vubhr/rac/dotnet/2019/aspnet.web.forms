﻿using demo.app._2.csharp.Models;
using demo.app._2.csharp.Persistence;
using demo.app._2.csharp.Persistence.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace demo.app._2.csharp
{
    public partial class Cart : System.Web.UI.Page
    {
        private List<CartProduct> _cartProducts;
        private IProductRepository _productsRepository;

        protected void Page_Load(object sender, EventArgs e)
        {
 
            _productsRepository = new SqlLiteProductRepository();
            if(!IsPostBack)
            {
                // Ovdje idu inicijalna punjenja podataka
            }

            // TO DO
            // Prebrojati distinct (npr. koliko ima id = 1, id = 2 itd.) i to posložiti u količinu, npr. id = 1, kol. 5
            GetCartData();
        }

        private void GetCartData()
        {
            _cartProducts = new List<CartProduct>();
            var products = ShoppingCart.Products;
            var distinctProducts = products.Select(p => p.Id).Distinct();
            foreach (var productId in distinctProducts)
            {
                var quantity = products.Where(x => x.Id == productId).Count();
                var productInfo = _productsRepository.GetById(productId);

                _cartProducts.Add(new CartProduct()
                {
                    Id = productId,
                    Quantity = quantity,
                    //Product = productInfo,
                    Price = quantity * productInfo.Price,
                    Title = productInfo.Title
                });
            }

            gvCart.DataSource = _cartProducts;
            gvCart.DataBind();
        }


        protected void btnRemoveFromShoppingCart_Click(object sender, EventArgs e)
        {
            var productId = (sender as Button).CommandArgument;
            ShoppingCart.RemoveFromCart(Convert.ToInt32(productId));
            GetCartData();
        }
    }
}