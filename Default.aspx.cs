﻿using demo.app._2.csharp.Models;
using demo.app._2.csharp.Persistence;
using demo.app._2.csharp.Persistence.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace demo.app._2.csharp
{
    public partial class _Default : Page
    {
        private IProductRepository _productsRepository;

        protected void Page_Load(object sender, EventArgs e)
        {
            _productsRepository = new SqlLiteProductRepository();

            if (!IsPostBack)
            {
                gvProducts.DataSource = _productsRepository.GetAll();
                gvProducts.DataBind();
            }

            var user = Session["user"] as User;
            pnlUserData.Visible = user != null;

            vubUserData.SelectedUser = user;
            vubUserData.OnSelectedUser += VubUserData_OnSelectedUser;
            //lblFirstName.Text = user?.FirstName;
            //lblLastName.Text = user?.LastName; 

 
        }

        private void VubUserData_OnSelectedUser(User selectedUser)
        {
           // throw new NotImplementedException();
        }

        protected void btnAddToShoppingCart_Click(object sender, EventArgs e)
        {
            var productId = (sender as Button).CommandArgument;
            ShoppingCart.AddToCart(new Product
            {
                Id = Convert.ToInt32(productId)
            });

            //_shoppingCart.Products.Add()


        }
    }
}