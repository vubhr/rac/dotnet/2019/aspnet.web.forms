﻿using demo.app._2.csharp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace demo.app._2.csharp
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            //if (HttpContext.Current != null && HttpContext.Current.Session != null)
            //{
            //    var user = HttpContext.Current.Session["user"] as User;
            //    if (user == null)
            //    {
            //        if (!HttpContext.Current.Request.Url.OriginalString.ToLower().Contains("login"))
            //        {
            //            Response.Redirect("~/Login");
            //            Response.End();
            //        }
            //    }
            //}
        }
    }
}