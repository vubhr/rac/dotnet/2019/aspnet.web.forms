﻿using demo.app._2.csharp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace demo.app._2.csharp
{
    public partial class UserData : System.Web.UI.UserControl
    {
        public User SelectedUser { get; set; }
        public delegate void SelectedUserEventHandler(User selectedUser);
        public event SelectedUserEventHandler OnSelectedUser;
        protected void Page_Load(object sender, EventArgs e)
        {
            var user = SelectedUser ?? (Session["user"] as User) ?? null;
            if(user != null)
            {
                lblFirstName.Text = user.FirstName;
                lblLastName.Text = user.LastName;
            }
        }

        protected void btnSelectUser_Click(object sender, EventArgs e)
        {
            OnSelectedUser?.Invoke(SelectedUser);
        }
    }
}