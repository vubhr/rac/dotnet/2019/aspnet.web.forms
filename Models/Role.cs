﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace demo.app._2.csharp.Models
{
    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}