﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace demo.app._2.csharp.Models
{
    public class CartProduct
    {
        public int Id { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public string Title { get; set; }
    }
}