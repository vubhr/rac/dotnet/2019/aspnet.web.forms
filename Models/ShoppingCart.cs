﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace demo.app._2.csharp.Models
{
    public class ShoppingCart
    {
        const string kljuc = "kosarica";
 
        public static void AddToCart(Product p)
        {
            var products = HttpContext.Current.Session[kljuc] as List<Product>;
            if(products != null)
            {
                products.Add(p);
                HttpContext.Current.Session[kljuc] = products;
            }
            else
            {
                var list = new List<Product>();
                list.Add(p);
                HttpContext.Current.Session[kljuc] = list;
            }
        }

        public static void RemoveFromCart(int productId)
        {
            var products = HttpContext.Current.Session[kljuc] as List<Product>;
            if (products != null)
            {
                var pro = products.Where(x => productId == x.Id).FirstOrDefault();
                products.Remove(pro);
                HttpContext.Current.Session[kljuc] = products;
            }
        }

        public static List<Product> Products 
        {
            get {
                var products = HttpContext.Current.Session[kljuc] as List<Product>;
                if(products != null)
                {
                    return products;
                }

                return new List<Product>();
            }
        } 
    }
}