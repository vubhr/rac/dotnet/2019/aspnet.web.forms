﻿using demo.app._2.csharp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo.app._2.csharp.Persistence.Abstractions
{
    public interface IUserRepository
    {
        User Login(string username, string password);
        User GetUserById(int userId);
        List<User> GetAll();

        void CreateUser(User user);
        void UpdateUser(User user);

        void DeleteUser(int userId);

        List<Role> GetRoles();
    }
}
