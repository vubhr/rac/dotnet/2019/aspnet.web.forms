﻿using demo.app._2.csharp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo.app._2.csharp.Persistence.Abstractions
{
    public interface IProductRepository
    {
        List<Product> GetAll();

        Product GetById(int id);
    }
}
