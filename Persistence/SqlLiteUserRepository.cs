﻿using demo.app._2.csharp.Models;
using demo.app._2.csharp.Persistence.Abstractions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Web;
using Dapper;

namespace demo.app._2.csharp.Persistence
{
    public class SqlLiteUserRepository : SqlLiteBaseRepository, IUserRepository
    {
        public void CreateUser(User user)
        {
            throw new NotImplementedException();
        }

        public SqlLiteUserRepository() : base()
        {
        }

        public void DeleteUser(int userId)
        {
            throw new NotImplementedException();
        }

        public List<User> GetAll()
        {
            // ADO.NET pristup
            var users = new List<User>();
            using (var connection = GetDbConnection())
            {
                connection.Open();
                var cmd = new SQLiteCommand(
                    $"SELECT id, username, firstName, lastName FROM users",
                   connection);

                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var user = new User();
                            user.Id = reader.GetInt32(0);
                            user.Username = reader.GetString(1);
                            user.FirstName = reader.GetString(2);
                            user.LastName = reader.GetString(3);
                            users.Add(user);
                        }
                    }
                }
            }

            return users;
        }

        public List<Role> GetRoles()
        {
            throw new NotImplementedException();
        }

        public User GetUserById(int userId)
        {
            // ADO.Net
            using (var connection = GetDbConnection())
            {
                connection.Open();

                var cmd = new SQLiteCommand(
                    $"SELECT id, username, firstName, lastName FROM users where id = @id",
                   connection);

                cmd.Parameters.AddWithValue("@id", userId);
                
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var user = new User();
                            user.Id = reader.GetInt32(0);
                            user.Username = reader.GetString(1);
                            user.FirstName = reader.GetString(2);
                            user.LastName = reader.GetString(3);
                            return user;
                        }
                    }
                    else
                    {
                        throw new Exception("No user found");
                    }
                }

                //var user = connection.Query<User>(
                //    $"SELECT id, username, firstName, lastName FROM users where id = @id",
                //   new { id = userId});

                
            }

            return null;
        }

        public User Login(string username, string password)
        {
            using (var connection = GetDbConnection())
            {
                connection.Open();

                //var user = connection.Query<User>(
                //   $"SELECT id, username, firstName, lastName FROM users where username =@username and " +
                //    $"password = @password",
                // new { username = username, password = password });

                var cmd = new SQLiteCommand(
                    $"SELECT id, username, firstName, lastName FROM users where username ='{username}' and " +
                    $"password = '{password}'",
                   connection);

                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var user = new User();
                            user.Id = reader.GetInt32(0);
                            user.Username = reader.GetString(1);
                            user.FirstName = reader.GetString(2);
                            user.LastName = reader.GetString(3);
                            return user;
                        }
                    }
                    else
                    {
                        throw new Exception("No user found");
                    }

                }
            }

            return null;
        }

        public void UpdateUser(User user)
        {
            throw new NotImplementedException();
        }
    }
}