﻿using demo.app._2.csharp.Models;
using demo.app._2.csharp.Persistence.Abstractions;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Web;
using Dapper;

namespace demo.app._2.csharp.Persistence
{

    public class SqlLiteUserDapperRepository : SqlLiteBaseRepository, IUserRepository
    {
        public void CreateUser(User user)
        {
            using (var connection = GetDbConnection())
            {
                connection.Open();
                connection.Execute("INSERT INTO users(username, firstName, lastName, role_id) VALUES (" +
                    "@username,  @firstName, @lastName, @roleId) ",
                    new
                    {
                        username = user.Username,
                        firstName = user.FirstName,
                        lastName = user.LastName,
                        roleId = user.Role.Id,
                        id = user.Id
                    });
            }
        }

        public void DeleteUser(int userId)
        {
            using (var connection = GetDbConnection())
            {
                try
                {
                    connection.Open();
                    var users = connection.Execute("UPDATE users set active = 0 where id = @id",
                        new { id = userId });

                }
                catch(Exception ex)
                {
                    // logiraj
                    throw;
                }
            }
        }

        public List<User> GetAll()
        {
            using (var connection = GetDbConnection())
            {
                connection.Open();
                var users = connection.Query<User>("SELECT id, username, firstName, lastName, role_id as RoleId FROM users where active = 1");
                users.Select(user => 
                    user.Role = GetRoles().Where(role => role.Id == user.RoleId).FirstOrDefault());

                return users.ToList();
            }
        }

        public List<Role> GetRoles()
        {
            using (var connection = GetDbConnection())
            {
                connection.Open();
                var roles = connection.Query<Role>("SELECT id, role as name from roles ");

                return roles.ToList();
            }
        }

        public User GetUserById(int userId)
        {
            using (var connection = GetDbConnection())
            {
                connection.Open();
                var user = connection.QueryFirstOrDefault<User>("SELECT id, username, firstName, lastName, role_id as RoleId " +
                    "FROM users where id = @id and active = 1", new { id = userId });

                return user;
            } 
        }

        public User Login(string username, string password)
        {
            using(var connection = GetDbConnection())
            {
                connection.Open();
                var user = connection.QueryFirstOrDefault<User>("SELECT id, username, firstName, lastName, role_id as RoleId " +
                    "FROM users where username = @username AND " +
                    "password = @password and active = 1",
                    new { username = username, password = password });

               
                user.Role = GetRoles().Where(role => role.Id == user.RoleId).FirstOrDefault();

                return user;
            }
        }

        public void UpdateUser(User user)
        {
            using (var connection = GetDbConnection())
            {
                connection.Open();
                connection.Execute("UPDATE users SET " +
                    "username = @username, firstName = @firstName, lastName = @lastName " +
                    "WHERE id = @id ",
                    new { username = user.Username, firstName = user.FirstName, lastName = user.LastName,
                    id = user.Id });

                
            }
           
        }
    }
}