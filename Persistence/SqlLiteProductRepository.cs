﻿using Dapper;
using demo.app._2.csharp.Models;
using demo.app._2.csharp.Persistence.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace demo.app._2.csharp.Persistence
{
    public class SqlLiteProductRepository : SqlLiteBaseRepository, IProductRepository
    {
        public List<Product> GetAll()
        {
            using (var connection = GetDbConnection())
            {
                connection.Open();
                var products = connection.Query<Product>("SELECT id, title, description, price, image FROM products");
        
                return products.ToList();
            }
        }

        public Product GetById(int id)
        {
            using (var connection = GetDbConnection())
            {
                connection.Open();
                var product = connection
                    .QueryFirstOrDefault<Product>
                    ("SELECT id, title, description, price, image FROM products " +
                    "where id = @id", new { id = id });

                return product;
            }
        }
    }
}