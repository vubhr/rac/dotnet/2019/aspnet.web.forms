﻿using demo.app._2.csharp.Models;
using demo.app._2.csharp.Persistence.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace demo.app._2.csharp.Persistence
{
    public class InMemoryUserRepository : IUserRepository
    {
        private List<User> _users { get; } = new List<User>();
        public InMemoryUserRepository()
        {
            _users.Add(new Models.User { Id = 1, Username = "admin", Password = "admin", FirstName = "Nebojša", LastName = "Pongračić" });
            _users.Add(new Models.User { Id = 2, Username = "pero", Password = "123", FirstName = "Pero", LastName = "Perić" });
            _users.Add(new Models.User { Id = 3, Username = "ivan", Password = "test", FirstName = "Ivan", LastName = "Horvat" });
        }

        public List<User> GetAll()
        {
            return _users;
        }

        public User GetUserById(int userId)
        {
            return _users.Where(user => user.Id == userId).FirstOrDefault();
        }

        public User Login(string username, string password)
        {
            return _users.Where(user => user.Username == username && user.Password == password).FirstOrDefault();
        }

        public void UpdateUser(User user)
        {
            throw new NotImplementedException();
        }

        public void DeleteUser(int userId)
        {
            throw new NotImplementedException();
        }

        public List<Role> GetRoles()
        {
            throw new NotImplementedException();
        }

        public void CreateUser(User user)
        {
            throw new NotImplementedException();
        }
    }
}