﻿using demo.app._2.csharp.Models;
using demo.app._2.csharp.Persistence;
using demo.app._2.csharp.Persistence.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace demo.app._2.csharp
{
    public partial class Admin : System.Web.UI.Page
    {
        private IUserRepository _usersRepository;

        protected void Page_Load(object sender, EventArgs e)
        {
            _usersRepository = new SqlLiteUserDapperRepository();
            var users = _usersRepository.GetAll();

            // Učitaj inicijalne podatke u grid samo kod prvog učitavanja stranice
            if (!IsPostBack)
            {
                ResetForm();
                GetUsersData();
                var roles = _usersRepository.GetRoles();

                foreach(var role in roles)
                {
                    ddlRole.Items.Add(new ListItem(role.Name, role.Id.ToString()));
                }
                // Možete i sa data source
                //ddlRole.DataSource = roles;
                //ddlRole.DataBind();
            }  
        }

        private void ResetForm()
        {
            lblFormTitle.Text = "Kreiraj novog korisnika";
            btnCreateUpdateUser.Text = "Create";

            txtLastName.Text = string.Empty;
            txtFirstName.Text = string.Empty;
            hdnUserId.Value = string.Empty;
            txtUsername.Text = string.Empty;
        }

        private void GetUsersData()
        {
            var users = _usersRepository.GetAll();

            gvUsers.DataSource = users;
            gvUsers.DataBind();
        }

        protected void btnEditUser_Click(object sender, EventArgs e)
        {
            var userId = (sender as Button).CommandArgument;
            var user = _usersRepository.GetUserById(Convert.ToInt32(userId));

            txtUsername.Text = user.Username;
            txtLastName.Text = user.LastName;
            txtFirstName.Text = user.FirstName;
            hdnUserId.Value = user.Id.ToString();

            lblFormTitle.Text = "Editiranje korisnika " + user.FirstName + " " + user.LastName;
            btnCreateUpdateUser.Text = "Edit";
        }

        protected void btnDeleteUser_Click(object sender, EventArgs e)
        {
            var userId = (sender as Button).CommandArgument;
            // Ovdje bi trebalo hendlati greške kod brisanja
            _usersRepository.DeleteUser(Convert.ToInt32(userId));

            GetUsersData();
        }

        protected void btnCreateUpdateUser_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(hdnUserId.Value))
            {
                // Editiraj korisnika
                var user = new User
                {
                    Username = txtUsername.Text,
                    FirstName = txtFirstName.Text,
                    LastName = txtLastName.Text,
                    Id = Convert.ToInt32(hdnUserId.Value),
                    //Role = 
                };

                _usersRepository.UpdateUser(user);
            }
            else
            {
                // Kreiraj korisnika
                var user = new User
                {
                    Username = txtUsername.Text,
                    FirstName = txtFirstName.Text,
                    LastName = txtLastName.Text,
                };

                _usersRepository.CreateUser(user);
            }

            GetUsersData();
            ResetForm();
        }


        
    }
}