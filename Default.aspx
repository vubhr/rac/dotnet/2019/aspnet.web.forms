﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" EnableEventValidation="false"
    AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="demo.app._2.csharp._Default" %>
<%--<%@ Register Src="~/UserData.ascx" TagPrefix="vub" TagName="UserData" %>--%>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Početna stranica</h1>

    <asp:Panel ID="pnlUserData" runat="server" Visible="false">
        <vub:UserData ID="vubUserData" runat="server"></vub:UserData>  
    </asp:Panel>
    Proizvodi u košarici: <%= demo.app._2.csharp.Models.ShoppingCart.Products.Count() %>
    <br />
    <asp:GridView runat="server" ID="gvProducts" AutoGenerateColumns="false">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" />
            <asp:BoundField DataField="Title" HeaderText="Naziv proizvoda" />
            <asp:BoundField DataField="Description" HeaderText="Opis proizvoda" />
            <asp:BoundField DataField="Price" HeaderText="Cijena" />
            <asp:TemplateField>
                <ItemTemplate>

                    <asp:Image ID="imgProductImage" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "Image") %>' 
                         Width="150px"
                        runat="server" />
                </ItemTemplate>
            </asp:TemplateField>       
          <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button ID="btnAddToShoppingCart" runat="server" Text="Dodaj u košaricu" CausesValidation="false"
                        OnClick="btnAddToShoppingCart_Click" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id") %>' />

                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <%--<asp:Button ID="btnLogout" runat="server" Text="Logout" OnClick="btnLogout_Click" />--%>
    <%= ViewState["test"] ?? "" %>
</asp:Content>
